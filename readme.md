ReTiled
=========
Import Tiled files into Unity to create 2D tile maps. Tiled is a popular open source tile map editor that can be found [here] [1]:.

Features
--
 - Multiple Formats supported including compressed
 - Full Tiled Property Support
 - Full Unity Inspector Support
 - Full Scripting Support

Tiled File Types:
--

Most other solutions only offer support for basic XML. We offer support for all of the full file types that Tiled offers at this time. Using a compressed XML format will reduce network traffic or disk space usage.

File Export Types:
 - JSON
 - XML (Base64, CSV, GZip, LUA, ZLib)

Map Types and Layers:
--

Tiled Imported supports orthogonal maps and multiple layers. The basic layer most commonly used is a Tile Layer. Other types can be used to draw background images or used to support custom scripting and events.

Map Layers:
 - Tile Layer
 - Object Layer
 - Image Layer


Properties:
--
Properties are the single most powerful part of what Tiled offers. With properties you can quickly set values in the editor to trigger complex reactions from within your game. Properties are set up in a hierarchy to drill down the function you want to create.

Property Types Supported:
 - Map Properties
 - Layer Properties
 - Object Properties
 - TileSet Properties
 - Tile Properties

Limitations:
--

Requried:
--
```sh
Tiled Map File in one of the above mentioned supported formats
Tiled Map Image Files used to create the map
(The Image files need to be placed in the resources folder as per normal Unity usage)
```

Installation
--
1. Purchase the Unity Package from the Unity Asset Store located here.
2. Import the Unity Package into your Unity Project.

Usage via Inspector:
--
1. Add a �Tiled Wizard� Prefab to a new scene. It requires a few things to start running. Provide the wizard with links to the follow items via the inspector search buttons. Or simply drag and drop them in place.
2. Hit the �Import Map� button in the inspector for the �Tiled Wizard� GameObject in your current scene.

Usage via Scripting:
--


Version
----
0.1

Contact
-----------
Email: 

  [1]: http://www.mapeditor.org/
  
    