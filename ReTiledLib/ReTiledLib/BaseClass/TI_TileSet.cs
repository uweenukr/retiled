﻿using System.Collections.Generic;

namespace ReTiledLib
{
    public class TI_TileSet
    {
        public string Name;
        public int TileSetID;
        public string ImagePath;
        public int ImageHeight;
        public int ImageWidth;
        public int Height;
        public int Width;
        public int Margin;
        public int Spacing;
        public int FirstGiD;

        public Dictionary<string, string> TileSetProperties;
        public Dictionary<string, Dictionary<string, string>> TileProperties;
    }
}
