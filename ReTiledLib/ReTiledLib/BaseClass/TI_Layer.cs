﻿using System.Collections.Generic;

namespace ReTiledLib
{
    public class TI_Layer
    {
        public string Name;
        public int LayerID;
        public string Type;
        public float Opacity;
        public int Height;
        public int Width;
        public int OffestX;
        public int OffsetY;
        public bool Visible;
        public string Image;

        public Dictionary<string, string> Properties;
        public List<int> TileData;
        public List<TI_Object> Objects;
    }
}
