﻿using System.Collections.Generic;

namespace ReTiledLib
{
    public class TI_Map
    {
        public string Name;
        public int Version;
        public int MapHeight;
        public int MapWidth;
        public int MapTileHeight;
        public int MapTileWidth;
        public string Orientation;

        public Dictionary<string, string> Properties;
        public List<TI_Layer> Layer;
        public List<TI_TileSet> TileSet;
    }
}
