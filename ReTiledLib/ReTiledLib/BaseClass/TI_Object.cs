﻿using System.Collections.Generic;

namespace ReTiledLib
{
    public class TI_Object
    {
        public string Name;
        public string Type;
        public int Height;
        public int Width;
        public int OffsetX;
        public int OffsetY;
        public bool Visible;

        public Dictionary<string, string> Properties;
    }
}
