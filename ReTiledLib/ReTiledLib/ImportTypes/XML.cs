﻿using System.IO;
using System.Xml.Serialization;

namespace ReTiledLib.Serializer
{
    public class XML<T>
    {
        public static T Deserialize(string xmlFilePath)
        {
            T retval = default(T);
            XmlSerializer deserializer = new XmlSerializer(typeof(T));
            using (TextReader textReader = new StreamReader(xmlFilePath))
            {
                retval = (T)deserializer.Deserialize(textReader);
                textReader.Close();
            }

            return retval;
        }

        public static T Deserialize(StreamReader streamReader)
        {
            T retval = default(T);
            XmlSerializer deserializer = new XmlSerializer(typeof(T));
            retval = (T)deserializer.Deserialize(streamReader);
            streamReader.Close();

            return retval;
        }
    }
}