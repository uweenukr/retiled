﻿using System.Collections.Generic;

namespace TiledImporter.Template
{
    public class JSONLayer
    {
        public List<int> Data { get; set; }
        public int Height { get; set; }
        public string Name { get; set; }
        public float Opacity { get; set; }
        public Dictionary<string, string> Properties { get; set; }
        public string Type { get; set; }
        public bool Visible { get; set; }
        public int Width { get; set; }
        public int X { get; set; }
        public int Y { get; set; }

        //Partial Object Support:
        public List<TiledObject> Objects { get; set; }
        public string Image { get; set; }
    }

    public class TiledObject
    {
        public int Height { get; set; }
        public string Name { get; set; }
        public Dictionary<string, string> Properties { get; set; }
        public string Type { get; set; }
        public bool Visible { get; set; }
        public int Width { get; set; }
        public int X { get; set; }
        public int Y { get; set; }
    }

    public class JSONTileSet
    {
        public int Firstgid { get; set; }
        public string Image { get; set; }
        public int ImageHeight { get; set; }
        public int ImageWidth { get; set; }
        public int Margin { get; set; }
        public string Name { get; set; }
        public Dictionary<string, string> Properties { get; set; }
        public int Spacing { get; set; }
        public int TileHeight { get; set; }
        public Dictionary<string, Dictionary<string, string>> TileProperties { get; set; }
        public int TileWidth { get; set; }
    }

    public class TiledJSON
    {
        public int Height { get; set; }
        public List<JSONLayer> Layers { get; set; }
        public string Orientation { get; set; }
        public Dictionary<string, string> Properties { get; set; }
        public int TileHeight { get; set; }
        public List<JSONTileSet> TileSets { get; set; }
        public int TileWidth { get; set; }
        public int Version { get; set; }
        public int Width { get; set; }
    }
}
