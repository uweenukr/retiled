﻿
namespace ReTiledLib.Serializer
{
    class Base64<T> : XML<T>
    {
        //Add Deserialize methods for sub types
    }

    class CSV<T> : XML<T>
    {

    }

    class GZip<T> : XML<T>
    {

    }

    class LUA<T> : XML<T>
    {

    }

    class ZLib<T> : XML<T>
    {

    }
}
