﻿using System.Collections.Generic;
using System.Xml.Serialization;

namespace ReTiledLib.Serializer
{
    [XmlRoot("map", Namespace = "")]
    public partial class TiledXML
    {
        [XmlAttribute("version")]
        public int Version { get; set; }

        [XmlAttribute("orientation")]
        public string Orientation { get; set; }

        [XmlAttribute("width")]
        public int Width { get; set; }

        [XmlAttribute("height")]
        public int Height { get; set; }

        [XmlAttribute("tilewidth")]
        public int Tilewidth { get; set; }

        [XmlAttribute("tileheight")]
        public int Tileheight { get; set; }

        [XmlElement("properties")]
        public Dictionary<string, string> PropertiesList { get; set; }

        [XmlElement("tileset")]
        public List<Tileset> TilesetList { get; set; }

        [XmlElement("layer")]
        public List<Layer> LayerList { get; set; }

        [XmlElement("objectgroup")]
        public List<Objectgroup> ObjectgroupList { get; set; }

        [XmlElement("imagelayer")]
        public List<Imagelayer> ImagelayerList { get; set; }

        [XmlText]
        public string XValue { get; set; }
    }

    [XmlRoot("properties", Namespace = "")]
    public partial class Properties
    {
        [XmlElement("property")]
        public List<Property> PropertyList { get; set; }

        [XmlText]
        public string XValue { get; set; }
    }

    [XmlRoot("property", Namespace = "")]
    public partial class Property
    {
        [XmlAttribute("name")]
        public string Name { get; set; }

        [XmlAttribute("value")]
        public string Value { get; set; }

        [XmlText]
        public string XValue { get; set; }
    }

    [XmlRoot("tileset", Namespace = "")]
    public partial class Tileset
    {
        [XmlAttribute("firstgid")]
        public int Firstgid { get; set; }

        [XmlAttribute("name")]
        public string Name { get; set; }

        [XmlAttribute("tilewidth")]
        public int Tilewidth { get; set; }

        [XmlAttribute("tileheight")]
        public int Tileheight { get; set; }

        [XmlElement("image")]
        public List<Image> ImageList { get; set; }

        [XmlElement("terraintypes")]
        public List<Terraintypes> TerraintypesList { get; set; }

        [XmlElement("tile")]
        public List<Tile> TileList { get; set; }

        [XmlText]
        public string XValue { get; set; }
    }

    [XmlRoot("image", Namespace = "")]
    public partial class Image
    {
        [XmlAttribute("source")]
        public string ImagePath { get; set; }

        [XmlAttribute("width")]
        public int Width { get; set; }

        [XmlAttribute("height")]
        public int Height { get; set; }

        [XmlText]
        public string XValue { get; set; }
    }

    [XmlRoot("terraintypes", Namespace = "")]
    public partial class Terraintypes
    {
        [XmlElement("terrain")]
        public List<Terrain> TerrainList { get; set; }

        [XmlText]
        public string XValue { get; set; }
    }

    [XmlRoot("terrain", Namespace = "")]
    public partial class Terrain
    {
        [XmlAttribute("name")]
        public string Name { get; set; }

        [XmlAttribute("tile")]
        public string Tile { get; set; }

        [XmlText]
        public string XValue { get; set; }
    }

    [XmlRoot("tile", Namespace = "")]
    public partial class Tile
    {
        [XmlAttribute("id")]
        public string Id { get; set; }

        [XmlAttribute("terrain")]
        public string Terrain { get; set; }

        [XmlElement("properties")]
        public List<Properties> PropertiesList { get; set; }

        [XmlText]
        public string XValue { get; set; }
    }

    [XmlRoot("layer", Namespace = "")]
    public partial class Layer
    {
        [XmlAttribute("name")]
        public string Name { get; set; }

        [XmlAttribute("width")]
        public int Width { get; set; }

        [XmlAttribute("height")]
        public int Height { get; set; }

        [XmlElement("properties")]
        public Dictionary<string, string> PropertiesList { get; set; }

        [XmlElement("data")]
        public List<int> DataList { get; set; }

        [XmlText]
        public string XValue { get; set; }
    }

    [XmlRoot("data", Namespace = "")]
    public partial class Data
    {
        [XmlElement("tile")]
        public List<Tile> TileList { get; set; }

        [XmlText]
        public string XValue { get; set; }
    }

    [XmlRoot("objectgroup", Namespace = "")]
    public partial class Objectgroup
    {
        [XmlAttribute("name")]
        public string Name { get; set; }

        [XmlAttribute("width")]
        public int Width { get; set; }

        [XmlAttribute("height")]
        public int Height { get; set; }

        [XmlAttribute("opacity")]
        public float Opacity { get; set; }

        [XmlText]
        public string XValue { get; set; }
    }

    [XmlRoot("imagelayer", Namespace = "")]
    public partial class Imagelayer
    {
        [XmlAttribute("name")]
        public string Name { get; set; }

        [XmlAttribute("width")]
        public string Width { get; set; }

        [XmlAttribute("height")]
        public string Height { get; set; }

        [XmlText]
        public string XValue { get; set; }
    }
}