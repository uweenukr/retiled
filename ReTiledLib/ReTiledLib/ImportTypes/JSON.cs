﻿using System;
using System.Text;
using System.Web.Script.Serialization;

namespace ReTiledLib.Serializer
{
    public class JSON
    {
        public static T Deserialize<T>(String jsonString)
        {
            try
            {
                var result = new JavaScriptSerializer().Deserialize<T>(jsonString);

                return result;
            }
            catch (Exception e)
            {
                Console.WriteLine(e);

                return default(T);
            }
        }

        public static T Deserialize<T>(byte[] jsonByteArray)
        {
            try
            {
                string s = Encoding.UTF8.GetString(jsonByteArray, 0, jsonByteArray.Length);

                return Deserialize<T>(s);
            }
            catch (Exception e )
            {
                Console.WriteLine(e);

                return default(T);
            }
            
        }

        public static T Deserialize<T>(string[] jsonStringArray)
        {
            try
            {
                string s = string.Join("", jsonStringArray);

                return Deserialize<T>(s);
            }
            catch (Exception e)
            {
                Console.WriteLine(e);

                return default(T);
            }
        }
    }
}
