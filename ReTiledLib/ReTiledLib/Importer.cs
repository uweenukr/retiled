﻿using System.Collections.Generic;
using ReTiledLib.Serializer;
using TiledImporter.Template;

namespace ReTiledLib
{
    public enum TiledType { Base64, CSV, GZip, JSON, LUA, XML, ZLib };

    public static class Importer
    {        
        public static TI_Map ReadTiledFromString(TiledType tiledType, string mapData)
        {
            TI_Map map = new TI_Map();

            switch (tiledType)
            {
                case TiledType.Base64:
                    //TiledXML base64;
                    //TiledMapFromJson(base64);
                    break;

                case TiledType.CSV:
                    //TiledXML csv;
                    //TiledMapFromJson(csv);
                    break;

                case TiledType.GZip:
                    //TiledXML gzip;
                    //TiledMapFromJson(gzip);
                    break;

                case TiledType.JSON:
                    TiledJSON json = JSON.Deserialize<TiledJSON>(mapData);
                    map = TiledMapFromJson(json);
                    break;

                case TiledType.LUA:
                    //TiledXML lua;
                    //TiledMapFromJson(lua);
                    break;

                case TiledType.XML:
                    //Need overload for raw string, bytearray, stringarray
                    //TiledXML xml = XML<TiledXML>.Deserialize(new StreamReader(mapData));
                    //TiledMapFromXML(xml);
                    break;

                case TiledType.ZLib:
                    //TiledXML zlib;
                    //TiledMapFromJson(zlib);
                    break;
            }

            return map;
        }

        #region PrivateConverters
        private static void TiledMapFromBase64(TiledXML base64)
        {
            
        }

        private static void TiledMapFromCSV(TiledXML csv)
        {

        }

        private static void TiledMapFromGZip(TiledXML gzip)
        {

        }

        private static TI_Map TiledMapFromJson(TiledJSON json)
        {
            TI_Map map = new TI_Map();

            //Base
            map.Version = json.Version;
            map.MapHeight = json.Height;
            map.MapWidth = json.Width;
            map.MapTileHeight = json.TileHeight;
            map.MapTileWidth = json.TileWidth;
            map.Orientation = json.Orientation;

            map.Properties = json.Properties;

            //Layer
            map.Layer = new List<TI_Layer>();
            
            for (int i = 0; i < json.Layers.Count; i++)
            {
                TI_Layer layertoAdd = new TI_Layer();
                layertoAdd.Name = json.Layers[i].Name;
                layertoAdd.Type = json.Layers[i].Type;
                layertoAdd.Opacity = json.Layers[i].Opacity;
                layertoAdd.Height = json.Layers[i].Height;
                layertoAdd.Width = json.Layers[i].Width;
                layertoAdd.OffestX = json.Layers[i].X;
                layertoAdd.OffsetY = json.Layers[i].Y;
                layertoAdd.Visible = json.Layers[i].Visible;

                layertoAdd.Properties = json.Layers[i].Properties;
                layertoAdd.TileData = json.Layers[i].Data;

                layertoAdd.Objects = new List<TI_Object>();
                if (json.Layers[i].Objects != null)
                {
                    foreach (TiledObject t in json.Layers[i].Objects)
                    {
                        TI_Object objects = new TI_Object();
                        objects.Name = t.Name;
                        objects.Type = t.Type;
                        objects.Height = t.Height;
                        objects.Width = t.Width;
                        objects.OffsetX = t.X;
                        objects.OffsetY = t.Y;
                        objects.Visible = t.Visible;

                        objects.Properties = t.Properties;

                        layertoAdd.Objects.Add(objects);
                    }
                }

                map.Layer.Add(layertoAdd);
            }

            //TileSet
            map.TileSet = new List<TI_TileSet>();
            for (int i = 0; i < json.TileSets.Count; i++)
            {
                TI_TileSet tileSettoAdd = new TI_TileSet();
                tileSettoAdd.Name = json.TileSets[i].Name;
                tileSettoAdd.ImagePath = json.TileSets[i].Image;
                tileSettoAdd.ImageHeight = json.TileSets[i].ImageHeight;
                tileSettoAdd.ImageWidth = json.TileSets[i].ImageWidth;
                tileSettoAdd.Height = json.TileSets[i].TileHeight;
                tileSettoAdd.Width = json.TileSets[i].TileWidth;
                tileSettoAdd.Margin = json.TileSets[i].Margin;
                tileSettoAdd.Spacing = json.TileSets[i].Spacing;
                tileSettoAdd.FirstGiD = json.TileSets[i].Firstgid;

                tileSettoAdd.TileSetProperties = json.TileSets[i].Properties;
                tileSettoAdd.TileProperties = json.TileSets[i].TileProperties;

                map.TileSet.Add(tileSettoAdd);
            }
            return map;
        }

        private static void TiledMapFromLUA(TiledXML lua)
        {

        }

        private static TI_Map TiledMapFromXML(TiledXML xml)
        {
            TI_Map map = new TI_Map();

            //Base
            map.Version = xml.Version;
            map.MapHeight = xml.Height;
            map.MapWidth = xml.Width;
            map.MapTileHeight = xml.Tileheight;
            map.MapTileWidth = xml.Tilewidth;
            map.Orientation = xml.Orientation;

            map.Properties = xml.PropertiesList;

            //Layer
            map.Layer = new List<TI_Layer>();
            for (int i = 0; i < xml.LayerList.Count; i++)
            {
                TI_Layer layertoAdd = new TI_Layer();
                layertoAdd.Name = xml.LayerList[i].Name;
                //layertoAdd.Type = xml;
                //layertoAdd.Opacity = xml;
                layertoAdd.Height = xml.LayerList[i].Height;
                layertoAdd.Width = xml.LayerList[i].Width;
                //layertoAdd.OffestX = xml;
                //layertoAdd.OffsetY = xml;
                //layertoAdd.Visible = xml;

                layertoAdd.Properties = xml.LayerList[i].PropertiesList;
                layertoAdd.TileData = xml.LayerList[i].DataList;

                map.Layer.Add(layertoAdd);
            }

            //TileSet
            map.TileSet = new List<TI_TileSet>();
            for (int i = 0; i < xml.TilesetList.Count; i++)
            {
                TI_TileSet tileSettoAdd = new TI_TileSet();
                tileSettoAdd.Name = xml.TilesetList[i].Name;
                //tileSettoAdd.ImagePath = xml;
                //tileSettoAdd.ImageHeight = xml;
                //tileSettoAdd.ImageWidth = xml;
                tileSettoAdd.Height = xml.TilesetList[i].Tileheight;
                tileSettoAdd.Width = xml.TilesetList[i].Tilewidth;
                //tileSettoAdd.Margin = xml;
                //tileSettoAdd.Spacing = xml;
                tileSettoAdd.FirstGiD = xml.TilesetList[i].Firstgid;

                //tileSettoAdd.TileSetProperties = xml.TilesetList[i].
                //tileSettoAdd.TileProperties = xml.TilesetList[i].

                map.TileSet.Add(tileSettoAdd);
            }
            return map;
        }

        private static void TiledMapFromZLib(TiledXML zlib)
        {

        }
        #endregion PrivateConverters
    }
}
