﻿using System.Collections.Generic;
using UnityEngine;
using ReTiledLib;

public class ReTiled : MonoBehaviour
{
    public TextAsset MapFile;
    public TiledType TiledType;
    public Texture2D[] TextureArray;

    public GameObject MapPrefab;
    public GameObject LayerPrefab;
    public GameObject ObjectPrefab;
    public GameObject TileSetPrefab;

    public bool UseFileAsName;
    public bool AutoCenterMap;

    #region Constructors
    public GameObject Import()
    {
        //Parse TIled Map File
        TI_Map importData = ParseTiled.FromTextAsset(TiledType, MapFile);

        return ProcessData(importData);
    }

    public GameObject Import(string mapfile)
    {
        //Parse TIled Map File
        TI_Map importData = ParseTiled.FromTextAsset(TiledType, MapFile);

        return ProcessData(importData);
    }

    public GameObject Import(TextAsset mapfile)
    {
        //Cache in case of non Inspector usage
        MapFile = mapfile;

        //Parse TIled Map File
        TI_Map importData = ParseTiled.FromTextAsset(TiledType, MapFile);

        return ProcessData(importData);
    }

    public GameObject Import(bool autoinit)
    {
        GameObject map = Import();
        Map mapcomp = map.GetComponent<Map>() as Map;
        mapcomp.DrawAllLayers();

        return map;
    }

    public GameObject Import(string mapfile, bool autoinit)
    {
        GameObject map = Import(mapfile);
        Map mapcomp = map.GetComponent<Map>() as Map;
        mapcomp.DrawAllLayers();

        return map;
    }

    public GameObject Import(TextAsset mapfile, bool autoinit)
    {
        GameObject map = Import(mapfile);
        Map mapcomp = map.GetComponent<Map>() as Map;
        mapcomp.DrawAllLayers();

        return map;
    }

    #endregion
    private GameObject ProcessData(TI_Map importdata)
    {
        int layerCount = 0;

        //Unity Game Object Creation
        GameObject map = Instantiate(MapPrefab) as GameObject;
        Map mapComp = map.GetComponent<Map>() as Map;

        if (UseFileAsName)
        {
            map.name = MapFile.name;
        }
        else
        {
            map.name = "Map";
        }

        mapComp.ConstructMap(importdata);

        //Layers
        mapComp.Layer = new List<Layer>();
        foreach (TI_Layer tiLayer in importdata.Layer)
        {
            //Unity Game Object Creation
            GameObject layer = Instantiate(LayerPrefab) as GameObject;
            layer.transform.parent = map.transform;
            Layer layerComp = layer.GetComponent<Layer>() as Layer;

            if (UseFileAsName)
            {
                layer.name = tiLayer.Name;
            }
            else
            {
                layer.name = "Layer" + layerCount;
            }

            layerComp.ConstructLayer(tiLayer, layerCount);

            //Objects
            layerComp.Objects = new List<Objects>();
            if (tiLayer.Objects != null)
            {
                int objectCount = 0;
                foreach (var tiObject in tiLayer.Objects)
                {
                    //Unity Game Object Creation
                    GameObject objects = Instantiate(ObjectPrefab) as GameObject;
                    objects.transform.parent = layer.transform;
                    Objects objectComp = objects.GetComponent<Objects>() as Objects;

                    if (UseFileAsName)
                    {
                        objects.name = tiObject.Name;
                    }
                    else
                    {
                        layer.name = "Object" + objectCount;
                    }

                    objectComp.ConstructObject(tiObject);

                    //Save GameObject and Component to Parent Lists
                    layerComp.ObjectsGO.Add(objects);
                    layerComp.Objects.Add(objectComp);
                    objectCount++;
                }
            }

            //Save GameObject and Component to Parent Lists
            mapComp.LayerGO.Add(layer);
            mapComp.Layer.Add(layerComp);
            layerCount++;
        }

        //TileSets
        mapComp.TileSet = new List<TileSet>();
        int tilesetCount = 0;
        foreach (var tiTileSet in importdata.TileSet)
        {
            //Unity Game Object Creation
            GameObject tileset = Instantiate(TileSetPrefab) as GameObject;
            tileset.transform.parent = map.transform;
            TileSet tileComp = tileset.GetComponent<TileSet>() as TileSet;

            if (UseFileAsName)
            {
                tileset.name = tiTileSet.Name;
            }
            else
            {
                tileset.name = "TileSet" + tilesetCount;
            }

            tileComp.ConstructTileSet(tiTileSet);

            //Save GameObject and Component to Parent Lists
            mapComp.TileSetGO.Add(tileset);
            mapComp.TileSet.Add(tileComp);
            tilesetCount++;
        }

        //Position Map
        if (AutoCenterMap)
        {
            map.transform.position = new Vector3(-(mapComp.MapWidth * mapComp.MapTileWidth) / 2, (mapComp.MapHeight * mapComp.MapTileHeight) / 2, 0);
        }

        //Return the Map Game Object
        return map;
    }
}