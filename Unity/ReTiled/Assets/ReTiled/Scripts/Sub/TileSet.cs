﻿using System.Collections.Generic;
using ReTiledLib;
using UnityEngine;

public class TileSet : MonoBehaviour
{
    public string Name;
    public string ImageName; //Add to lib
    public string ImagePath; //Should be set to Unity Resources path
    public int ImageHeight;
    public int ImageWidth;
    public int Height; //Rename to TileHeight
    public int Width; // Rename to TileWidth
    public int Margin;
    public int Spacing;
    public int FirstGID;

    public Texture2D Texture;

    public List<Properties> Properties = new List<Properties>();
    public List<TilePropertiesNest> TileProperties = new List<TilePropertiesNest>();

    public void ConstructTileSet(TI_TileSet tileset)
    {
        Name = tileset.Name;
        ImagePath = tileset.ImagePath;
        ImageHeight = tileset.ImageHeight;
        ImageWidth = tileset.ImageWidth;
        Height = tileset.Height;
        Width = tileset.Width;
        Margin = tileset.Margin;
        Spacing = tileset.Spacing;
        FirstGID = tileset.FirstGiD;

        //TileSet Properties
        if (tileset.TileSetProperties != null)
        {
            foreach (KeyValuePair<string, string> t in tileset.TileSetProperties)
            {

                Properties.Add(new Properties(t.Key, t.Value));
            }
        }

        //if (tiTileSet.TileProperties != null)
        //{
        //    foreach (var t in tiTileSet.TileProperties)
        //    {
        //        TilePropertiesNest newNest = new TilePropertiesNest();
        //        if (t.Value != null)
        //        {
        //            foreach (var u in t.Value)
        //            {
        //                newNest.TileProperties.Add(new Properties(u.Key, u.Value));
        //            }
        //            newNest.Name = t.Key;
        //        }
        //        tileComp.TileProperties.Add(newNest);
        //    }
        //}

        FindTexture();
    }

    public void FindTexture()
    {
        //Texture

        //if unable to find 
    }
}
