﻿using System.Collections.Generic;
using System.IO;
using ReTiledLib;
using UnityEditor;
using UnityEngine;

public class Layer : MonoBehaviour
{
    public string Name;
    public int LayerID;
    public string Type;
    public float Opacity;
    public int Height;
    public int Width;
    public int OffestX;
    public int OffsetY;
    private bool Visible; //Use Unity Set Active. Only used for map importing
    public string Image;

    public List<Properties> Properties = new List<Properties>();
    public List<int> TileData;
    public List<Objects> Objects;
    public List<GameObject> ObjectsGO;
    public List<TileSet> LayerTileSets;

    public Texture2D Texture;

    private Map _parentMap;
    private int _vertexCount;
    private int _usedVertices;

    public void ConstructLayer(TI_Layer layer, int layerID)
    {
        Name = layer.Name;
        LayerID = layerID;
        Type = layer.Type;
        Opacity = layer.Opacity;
        Height = layer.Height;
        Width = layer.Width;
        OffestX = layer.OffestX;
        OffsetY = layer.OffsetY;
        Visible = layer.Visible;

        //LayerProperties
        if (layer.Properties != null)
        {
            foreach (KeyValuePair<string, string> pair in layer.Properties)
            {
                Properties.Add(new Properties(pair.Key, pair.Value));
            }
        }

        TileData = layer.TileData;

        if(!Visible)
        {
            gameObject.SetActive(false); //Default is active
        }

        //Cache Parent Map Class
        _parentMap = transform.parent.gameObject.GetComponent<Map>() as Map;
    }

    public void DrawLayer()
    {
        //Layer has no tile data to draw
        if (TileData == null)
        {
            return;
        }

        AddMesh();
        AddMaterial();
    }

    public void AddMesh()
    {
        Mesh mesh = new Mesh();
        Layer currentLayer = _parentMap.Layer[LayerID];
        List<Vector3> vertices = new List<Vector3>();
        List<Vector2> uv = new List<Vector2>();
        List<int> triangles = new List<int>();
        // UsedVertices is used to maintain a count of the vertices between 
        //layers so when you call renderTriangles, it nows where to start.
        _usedVertices = 0;
        _vertexCount = 0;

        //Get TileSets this layer uses
        LayerTileSets = GetAllTileSets();

        vertices.AddRange(currentLayer.RenderVertices());
        uv.AddRange(currentLayer.RenderUv());
        triangles.AddRange(currentLayer.renderTriangles(_usedVertices, _usedVertices + currentLayer.vertexCount));

        mesh.vertices = vertices.ToArray();
        mesh.uv = uv.ToArray();
        mesh.triangles = triangles.ToArray();

        //Assign Mesh to Layer
        MeshFilter filter = currentLayer.GetComponent<MeshFilter>();
        filter.mesh = mesh;
    }

    public void AddMaterial()
    {
        Material material = new Material(Shader.Find("Mobile/Particles/Alpha Blended"));
        string assetpath = "Assets/" + _parentMap.Name + "/";

        if (!Directory.Exists(assetpath))
        {
            AssetDatabase.CreateFolder("Assets", _parentMap.Name);
        }

        AssetDatabase.CreateAsset(material, assetpath + Name + ".mat");
        gameObject.renderer.material = material;
    }

    public void AddTexture(Texture2D texture)
    {
        Texture = texture; //Cache

        transform.renderer.material.SetTexture("_MainTex", Texture); //And Set

        //Expanded version:
        //Look up the list of tiles: LayerTileSets
        //
    }

    #region Private Methods
    // Renders the tile vertices.
    // Basically, it reads the entire CSV file cells, and creates a 
    // 4 vertexes (forming a rectangle or square according to settings) 
    // when a value different than 0 is found  
    private List<Vector3> RenderVertices()
    {
        int dataIndex = 0;
        float z = LayerID * -10;
        List<Vector3> vertices = new List<Vector3>();
        for (int i = 0; i < Height; i++)
        {
            for (int j = 0; j < Width; j++)
            {
                int dataValue = TileData[dataIndex];
                int currentTileSet = FindTileSetForTile(dataIndex);
                if (dataValue != 0)
                {
                    vertices.AddRange(new Vector3[] {
							new Vector3 (LayerTileSets[currentTileSet].Width * (j + 1)   , LayerTileSets[currentTileSet].Height * (-i + 1)    , z),
							new Vector3 (LayerTileSets[currentTileSet].Width * (j + 1)   , LayerTileSets[currentTileSet].Height * -i          , z),							
							new Vector3 (LayerTileSets[currentTileSet].Width * j         , LayerTileSets[currentTileSet].Height * (-i + 1)    , z),								
							new Vector3 (LayerTileSets[currentTileSet].Width * j         , LayerTileSets[currentTileSet].Height * -i          , z)
						});
                    _vertexCount += 4;
                }
                dataIndex++;
            }
        }
        return vertices;
    }

    // Creates the Face UV for the faces according to the tile represented on the TMX.
    private List<Vector2> RenderUv()
    {
        List<Vector2> uv = new List<Vector2>();
        int horizontalCellCount = LayerTileSets[0].ImageWidth / (LayerTileSets[0].Width + LayerTileSets[0].Spacing);
        int verticalCellCount = LayerTileSets[0].ImageHeight / (LayerTileSets[0].Height + LayerTileSets[0].Spacing);
        float cellWidth = ((float)LayerTileSets[0].Width / LayerTileSets[0].ImageWidth);
        float cellHeight = ((float)LayerTileSets[0].Height / LayerTileSets[0].ImageHeight);
        float borderWidth = ((float)LayerTileSets[0].Spacing / LayerTileSets[0].ImageWidth);
        float borderHeight = ((float)LayerTileSets[0].Spacing / LayerTileSets[0].ImageHeight);
        int totalCells = Width * Height;
        int dataValue;
        for (int i = 0; i < totalCells; i++)
        {
            dataValue = TileData[i];
            if (dataValue != 0)
            {
                dataValue = dataValue - LayerTileSets[0].FirstGID;
                int posY = dataValue / verticalCellCount;
                int posX = dataValue % horizontalCellCount;
                float u = ((cellWidth + borderWidth) * posX) + borderWidth / 2;
                float v = 1.0f - ((cellHeight + borderHeight) * posY) - borderHeight / 2;

                uv.AddRange(new Vector2[] {
						new Vector2 (u + cellWidth, v),
						new Vector2 (u + cellWidth, v - cellHeight),
						new Vector2 (u, v),
						new Vector2 (u, v - cellHeight)					
					});
            }
        }
        return uv;
    }

    // Creates the triangles given the ammount of the Used Vertices until now (including other layers).
    private List<int> renderTriangles(int start, int end)
    {
        List<int> triangles = new List<int>();
        int currentTri = start;
        while (currentTri < end)
        {
            triangles.AddRange(new int[] {
						currentTri, currentTri + 1, currentTri + 2,
			            currentTri + 2, currentTri + 1, currentTri + 3
					});
            currentTri += 4;
        }
        return triangles;
    }

    //Used to find the tilesheet that should be referenced for the current tile
    private int FindTileSetForTile(int tileID)
    {
        if (LayerTileSets.Count > 1)
        {
            for (int i = 0; i < LayerTileSets.Count; i++)
            {
                if (tileID >= LayerTileSets[i].FirstGID && tileID < LayerTileSets[i + 1].FirstGID)
                {
                    return i;
                }
            }
        }
        return 0;
    }

    private List<TileSet> GetAllTileSets()
    {
        List<TileSet> tilesetlist = new List<TileSet>();

        foreach (TileSet tileset in _parentMap.TileSet)
        {
            tilesetlist.Add(tileset);
        }

        return tilesetlist;
    }

    private int vertexCount
    {
        get
        {
            return this._vertexCount;
        }
    }
    #endregion
}