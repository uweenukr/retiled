﻿using System.Collections.Generic;
using ReTiledLib;
using UnityEngine;

public class Objects : MonoBehaviour
{
    public string Name;
    public string Type;
    public int Height;
    public int Width;
    public int OffsetX;
    public int OffsetY;
    public bool Visible;

    public List<Properties> Properties = new List<Properties>();

    public void ConstructObject(TI_Object tiobject)
    {
        Name = tiobject.Name;
        Type = tiobject.Type;
        Height = tiobject.Height;
        Width = tiobject.Width;
        OffsetX = tiobject.OffsetX;
        OffsetY = tiobject.OffsetY;
        Visible = tiobject.Visible;

        if (tiobject.Properties != null)
        {
            foreach (KeyValuePair<string, string> t in tiobject.Properties)
            {
                Properties.Add(new Properties(t.Key, t.Value));
            }
        }
    }
}
