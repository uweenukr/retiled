﻿using ReTiledLib;
using UnityEngine;

public static class ParseTiled
{
    public static TI_Map FromTextAsset(TiledType tiledtype, TextAsset tiledasset) { return FromString(tiledtype, tiledasset.ToString()); }
    public static TI_Map FromString(TiledType tiledtype, string tiledtext)
    {
        if (tiledtext == null)
        {
            Debug.LogError("MapFile is missing or not a valid TextAsset.");
            return null;
        }
        return Importer.ReadTiledFromString(tiledtype, tiledtext);
    }
}

