﻿using System;
using System.Collections.Generic;

[Serializable]
public class Properties
{
    public string property;
    public string value;
    public Properties(string property, string value)
    {
        this.property = property;
        this.value = value;
    }
}

[Serializable]
public class TilePropertiesNest
{
    public string TiledID;
    public List<Properties> TileProperties;
}
