﻿using System.Collections.Generic;
using UnityEngine;
using ReTiledLib;

public class Map : MonoBehaviour
{
    public string Name;
    public int Version;
    public int MapHeight;
    public int MapWidth;
    public int MapTileHeight;
    public int MapTileWidth;
    public string Orientation;

    public List<Properties> Properties = new List<Properties>();
    public List<Layer> Layer;
    public List<GameObject> LayerGO;
    public List<TileSet> TileSet;
    public List<GameObject> TileSetGO;

    public Texture2D[] TextureArray;

    public void ConstructMap(TI_Map mapData)
    {
        Name = gameObject.name;
        Version = mapData.Version;
        MapHeight = mapData.MapHeight;
        MapWidth = mapData.MapWidth;
        MapTileHeight = mapData.MapTileHeight;
        MapTileWidth = mapData.MapTileWidth;
        Orientation = mapData.Orientation;

        if (mapData.Properties != null)
        {
            foreach (KeyValuePair<string, string> t in mapData.Properties)
            {
                Properties.Add(new Properties(t.Key, t.Value));
            }
        }
    }

    public void DrawLayer(int layerID)
    {
        Layer[layerID].DrawLayer();
    }

    public void DrawAllLayers()
    {
        for (int layerID = 0; layerID < LayerGO.Count; layerID++)
        {
            DrawLayer(layerID);
        }
    }
}