return {
  version = "1.1",
  luaversion = "5.1",
  orientation = "orthogonal",
  width = 10,
  height = 10,
  tilewidth = 20,
  tileheight = 20,
  properties = {
    ["MapProperty"] = "1"
  },
  tilesets = {
    {
      name = "floor-tiles-20x20",
      firstgid = 1,
      tilewidth = 20,
      tileheight = 20,
      spacing = 0,
      margin = 0,
      image = "floor-tiles-20x20.png",
      imagewidth = 400,
      imageheight = 260,
      properties = {},
      tiles = {
        {
          id = 0,
          properties = {
            ["Tile0Property"] = "1"
          }
        },
        {
          id = 1,
          properties = {
            ["Tile2Property"] = "1"
          }
        },
        {
          id = 2,
          properties = {
            ["Tile3Property"] = "1"
          }
        },
        {
          id = 46,
          properties = {
            ["collidable"] = "1"
          }
        }
      }
    },
    {
      name = "floor-tiles-20x20",
      firstgid = 261,
      tilewidth = 20,
      tileheight = 20,
      spacing = 0,
      margin = 0,
      image = "floor-tiles-20x20.png",
      imagewidth = 400,
      imageheight = 260,
      properties = {},
      tiles = {
        {
          id = 104,
          properties = {
            ["tilesettwoprop"] = "true"
          }
        }
      }
    }
  },
  layers = {
    {
      type = "tilelayer",
      name = "Tile Layer 1",
      x = 0,
      y = 0,
      width = 10,
      height = 10,
      visible = true,
      opacity = 1,
      properties = {
        ["LayerProperty"] = "1"
      },
      encoding = "lua",
      data = {
        47, 47, 47, 47, 47, 47, 47, 47, 47, 47,
        47, 1, 1, 1, 1, 1, 1, 1, 251, 47,
        47, 47, 1, 1, 1, 241, 241, 1, 1, 47,
        1, 47, 1, 1, 1, 241, 241, 1, 1, 47,
        1, 47, 1, 1, 1, 1, 241, 1, 1, 47,
        1, 47, 1, 1, 1, 1, 241, 1, 1, 47,
        47, 47, 1, 1, 1, 241, 241, 1, 1, 47,
        47, 1, 1, 1, 1, 241, 1, 1, 1, 47,
        47, 251, 1, 1, 1, 47, 47, 47, 47, 47,
        47, 47, 47, 47, 47, 47, 1, 1, 1, 1
      }
    },
    {
      type = "tilelayer",
      name = "Tile Layer 2",
      x = 0,
      y = 0,
      width = 10,
      height = 10,
      visible = false,
      opacity = 0.55,
      properties = {},
      encoding = "lua",
      data = {
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 65, 65, 65, 65, 65, 65, 0,
        0, 0, 0, 65, 65, 0, 0, 0, 65, 0,
        0, 0, 0, 65, 0, 0, 0, 65, 65, 0,
        0, 0, 0, 65, 65, 65, 65, 65, 0, 0,
        0, 0, 0, 65, 65, 65, 65, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0
      }
    },
    {
      type = "objectgroup",
      name = "Object Layer 1",
      visible = true,
      opacity = 0.45,
      properties = {},
      objects = {}
    },
    {
      type = "imagelayer",
      name = "Image Layer 1",
      visible = true,
      opacity = 1,
      image = "",
      properties = {}
    }
  }
}
