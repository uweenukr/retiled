﻿using UnityEditor;
using UnityEngine;
using System.Collections;

[CustomEditor(typeof(Layer))]
public class LayerEditorExtension : Editor {

    public override void OnInspectorGUI()
    {
        Layer layer = (Layer)target;

        if (GUILayout.Button("Draw Layer"))
        {
            layer.DrawLayer();
        }

        DrawDefaultInspector();
    }
}
