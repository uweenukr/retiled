﻿using UnityEditor;
using UnityEngine;

[CustomEditor(typeof(Map))]
public class MapEditorExtension : Editor {

    public override void OnInspectorGUI()
    {
        Map map = (Map)target;

        if (GUILayout.Button("Draw Layers"))
        {
            map.DrawAllLayers();
        }

        DrawDefaultInspector();
    }
}
