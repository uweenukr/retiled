﻿using System.Diagnostics;
using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(ReTiled))]
public class ReTiledWizardEditor : Editor
{
    public override void OnInspectorGUI()
    {
        ReTiled map = (ReTiled)target;

        DrawDefaultInspector();

        if (GUILayout.Button("Import + Setup"))
        {
            map.Import(map.MapFile, true);
        }

        if (GUILayout.Button("Import Only"))
        {
            map.Import(map.MapFile);
        }
    }
}
